import requests


class Deck:
    """
    deck of card
    """
    deck={}

    def __init__(self,nb_paquets="6"):
        requete = requests.get("https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count="+nb_paquets)
        self.deck=requete.json()

    def draw(self,nb_cartes="1"):
        """
        Draw a card from the deck
        """
        requete = requests.get("https://deckofcardsapi.com/api/deck/"+self.deck["deck_id"]+"/draw/?count="+nb_cartes)
        self.deck=requete.json()
        return requete.json()["cards"]
    
    def shuffle(self,remaining=False):
        """
        shuffle the deck
        """
        if remaining:
            remaining="?remaining=true"
        else:
            remaining=""
        requete = requests.get("https://deckofcardsapi.com/api/deck/"+self.deck["deck_id"]+"/shuffle/"+remaining)
        self.deck=requete.json()

