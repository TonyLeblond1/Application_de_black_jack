class Hand:
    """
    Hand of card
    """
    cardlist=[]

    def __init__(self):
        self.cardlist=[]
    
    def add(self,card):
        """
        add a card to the Hand
        """
        self.cardlist.append(card)
    
    def emptyHand(self):
        """
        empty the Hand
        """
        self.cardlist=[]
    
    def display(self):
        """
        Show cards in hand
        """
        for card in self.cardlist:
            print(card[0]['code'])
    
    def display_partial(self):
        """
        Only shows the second card in hand (for croupier starting hand)
        """
        print(self.cardlist[1][0]['code'])
    
    def total(self):
        """
        sum the total of the Hand
        """
        sum=0
        ace=0
        for card in self.cardlist:
            if card[0]["value"]=='KING' or card[0]["value"]=='QUEEN' or card[0]["value"]=='JACK':
                sum+=10
            elif card[0]["value"]=="ACE":
                sum+=1
                ace+=1
            else:
                sum+=int(card[0]["value"])
        for i in range(ace):
            if sum+10<22:
                sum+=10
        return sum
