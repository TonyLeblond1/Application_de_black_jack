import requests
from model.deck import Deck
from model.joueur import Joueur
from dao_bdd.liste_joueur import Liste_joueur
from dao_bdd.inscription import Inscription
from dao_bdd.maj import Maj

class Jeu:
    def __init__(self):
        login="default"
        while login!="e" and login!="i":
            if login!="e" and login!="i":
                login=input("Voulez vous vous inscrire ou choisir un compte existant ? (i/e) ")
        
        if login == "i":
            nom=input("votre nom : ")
            solde=input("votre solde : ")
            while(int(solde)<0):
                solde=input("votre solde doit être supperieur à 0 : ")
            self.player=Joueur(nom,solde)
            Inscription(self.player)

        else:
            print("Qui etes vous ?")
            for Joueurlist in Liste_joueur.getList():
                print(Joueurlist["nom_joueur"])
            nom=input()
            for Joueurlist in Liste_joueur.getList():
                if nom==Joueurlist["nom_joueur"]:
                    solde=Joueurlist["solde"]
            self.player=Joueur(nom,solde)
        
        self.deck=Deck()
        self.croupier=Joueur("croupier","infini")
        
    def distribuer_cartes(self):
        """
        set starting hands
        """
        self.player.hand.emptyHand()
        self.croupier.hand.emptyHand()
        self.deck.shuffle()
        self.player.hand.add(self.deck.draw())
        self.croupier.hand.add(self.deck.draw())
        self.player.hand.add(self.deck.draw())
        self.croupier.hand.add(self.deck.draw())

        
    def jouer(self):
        """
        Game of black jack
        """
        perdu=False
        self.distribuer_cartes()
        choix="o"
        while not perdu and choix!="n":
            print("Main du joueur : ")
            self.player.hand.display()
            print("total : "+str(self.player.hand.total()))
            print("Main du croupier : ")
            self.croupier.hand.display_partial()
            choix = input("Voulez-vous tirer une carte ? (o/n) : ")
            if choix == "o":
                self.player.hand.add(self.deck.draw())
                if self.player.hand.total() > 21:
                    print("Vous avez dépassé 21. Vous avez perdu.")
                    solde=int(self.player.solde) -10
                    self.player.solde = str(solde)
                    perdu=True
        if not perdu:
            while self.croupier.hand.total() < 17:
                self.croupier.hand.add(self.deck.draw())
            print("Main du joueur : ")
            self.player.hand.display()
            print("Total de la main du joueur : ", self.player.hand.total())
            print("Main du croupier : ")
            self.croupier.hand.display()
            print("Total de la main du croupier : ", self.croupier.hand.total())
            if self.croupier.hand.total() > 21:
                print("Le croupier a dépassé 21. Vous avez gagné !")
                solde=int(self.player.solde) +10
                self.player.solde = str(solde)
            elif self.croupier.hand.total() > self.player.hand.total():
                print("Le croupier a une main plus forte, vous perdez")
                solde=int(self.player.solde) -10
                self.player.solde = str(solde)
        Maj(self.player)
        print("votre nouveau solde : "+ self.player.solde)

                