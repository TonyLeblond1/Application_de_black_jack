from model.hand import Hand


class Joueur:
    nom=""
    solde=""
    hand=[]

    def __init__(self, nom, solde):
        self.nom = nom
        self.solde = solde
        self.hand=Hand()
    