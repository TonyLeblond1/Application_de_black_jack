import unittest

from model.hand import Hand


class HandTest(unittest.TestCase):

    def test_add(self):
        expected=[{
            "code": "6H", 
            "image": "https://deckofcardsapi.com/static/img/6H.png", 
            "images": {
                          "svg": "https://deckofcardsapi.com/static/img/6H.svg", 
                          "png": "https://deckofcardsapi.com/static/img/6H.png"
                      }, 
            "value": "6", 
            "suit": "HEARTS"
        }]
        main=Hand()
        card={
            "code": "6H", 
            "image": "https://deckofcardsapi.com/static/img/6H.png", 
            "images": {
                          "svg": "https://deckofcardsapi.com/static/img/6H.svg", 
                          "png": "https://deckofcardsapi.com/static/img/6H.png"
                      }, 
            "value": "6", 
            "suit": "HEARTS"
        }
        actual=main.add(card)
        self.assertEqual(expected, actual.cardlist)

if __name__ == "__main__":
    HandTest.test_add()