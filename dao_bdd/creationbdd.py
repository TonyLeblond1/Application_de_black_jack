from dao_bdd.dbconnection import DBConnection

class Creation_bdd():
    def creation_bdd(self):
        with open('db_init.sql','r') as f : 
            sqlFile = f.read()
        conn = DBConnection().connection
        with conn.cursor() as cursor:
            cursor.execute(sqlFile)
            conn.commit()

if __name__ == "__main__":
    Creation_bdd.creation_bdd()
