import os
#import dotenv
import psycopg2
from psycopg2.extras import RealDictCursor


class DBConnection():
    """
    Technical class to open only one connection to the DB.
    """
    def __init__(self):
        """
        Change here to acces to your own database
        """
        self.__connection =psycopg2.connect(
            password="id1947",
            host="sgbd-eleves.domensai.ecole",
            port="5432",
            database="id1947",
            user="id1947",

            cursor_factory=RealDictCursor)

    @property
    def connection(self):
        """
        return the opened connection.

        :return: the opened connection.
        """
        return self.__connection
