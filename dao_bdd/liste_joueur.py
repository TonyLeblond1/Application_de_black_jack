from dao_bdd.dbconnection import DBConnection

class Liste_joueur:
    def getList():
        """
        Get the list of player in the database
        """
        conn = DBConnection().connection
        requete = "SELECT * FROM joueur"
        with conn.cursor() as cursor:
            cursor.execute(requete)
            res = cursor.fetchall()
            sortie_json = []
            for row in res:
                sortie_json.append({'nom_joueur':str(row['nom_joueur']),'solde':row['solde']})
            return sortie_json
