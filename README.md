# Application De Black Jack
This application aims to play a game of black Jack. It allows player to sign up by giving a name and solde or login if they are already registered. After login the game start. Win a game will earn you 10 of solde and a loose make you lose 10 of solde.


WARNING : 
- this application was tested using a PostGreSQL database, use another database at your own risk.
- This application work with the database of ENSAI. You must execute it with a connection from ensai (VM).
If you want to use your own data base Fill in the informations necessary to connect to your database in the dao_bdd/dbconnection.py and create a table joueur(nom_joueur VAR(50),solde VAR(50)) or you can run creationbdd.py in the file dao_bdd to create the database.

USER GUIDE :

- STEP 1 : Install all the required packages (pip install -r requirements.txt)
- STEP 2 : run python main.py
- STEP 3 : play in terminal

The class tested are : hand.py
Test can be run by running hand_test.py