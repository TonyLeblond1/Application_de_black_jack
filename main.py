import requests
from model.deck import Deck
from model.joueur import Joueur
from dao_bdd.inscription import Inscription
from dao_bdd.liste_joueur import Liste_joueur
from model.jeu import Jeu

if __name__ == "__main__":
    game=Jeu()
    encore="o"
    while encore == "o" and int(game.player.solde)>9:
        game.jouer()
        encore= input("voulez vous encore jouer ?")
    if int(game.player.solde)<10:
        print("votre solde est insuffisant pour jouer")


